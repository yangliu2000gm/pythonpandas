# .iloc[]
%%timeit
norm_prices = np.zeros(len(listings,))
for i in range(len(listings)):
    norm_prices[i] = (listings.iloc[i]['price'] - min_price) / (max_price - min_price)
listings['norm_price'] = norm_prices
# 8.91 s ± 479 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)

# Iterrows()
%%timeit
norm_prices = np.zeros(len(listings,))
for i, row in listings.iterrows():
    norm_prices[i] = (row['price'] - min_price) / (max_price - min_price)
listings['norm_price'] = norm_prices
# 3.99 s ± 346 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)

# .loc[]
%%timeit
norm_prices = np.zeros(len(listings,))
for i in range(len(norm_prices)):
    norm_prices[i] = (listings.loc[i, 'price'] - min_price) / (max_price - min_price)
listings['norm_price'] = norm_prices
# 408 ms ± 61.2 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)

# .map()
%%timeit 
listings['norm_price'] = listings['price'].map(lambda x: (x - min_price) / (max_price - min_price))
# 39.8 ms ± 2.33 ms per loop (mean ± std. dev. of 7 runs, 10 loops each)

# Vectorize
%%timeit
listings['norm_price'] = (listings['price'] - min_price) / (max_price - min_price)
# 1.76 ms ± 107 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)